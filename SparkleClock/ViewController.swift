//
//  ViewController.swift
//  SparkleClock
//
//  Created by ANDROMEDA on 7/15/16.
//  Copyright © 2016 infancyit. All rights reserved.
//

import UIKit
import QuartzCore

class ViewController: UIViewController {
  var timer: NSTimer!
  var dateFormatter: NSDateFormatter!
  @IBOutlet var clockLabel: UILabel!
  @IBOutlet var tapGestureRecognizer: UITapGestureRecognizer!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    timer = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: #selector(ViewController.updateClock), userInfo: nil, repeats: true)
    dateFormatter = NSDateFormatter()
    dateFormatter.dateStyle = NSDateFormatterStyle.NoStyle
    dateFormatter.timeStyle = NSDateFormatterStyle.MediumStyle
  }
  
  override func viewWillAppear(animated: Bool) {
    updateClock()
  }
  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    
    UIApplication.sharedApplication().idleTimerDisabled = true
  }
  
  override func viewDidDisappear(animated: Bool) {
    super.viewDidDisappear(animated)
    
    UIApplication.sharedApplication().idleTimerDisabled = false
  }
  
  override func prefersStatusBarHidden() -> Bool {
    return true
  }
  
  func updateClock() {
    let timeToDisplay = dateFormatter.stringFromDate(NSDate())
    clockLabel.text = timeToDisplay
  }
  
  @IBAction func didTapView() {
    tapGestureRecognizer.enabled = false
    
    UIView.animateWithDuration(0.25, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: .CurveEaseIn, animations: {
      self.clockLabel.transform = CGAffineTransformMakeScale(1.2, 1.2)
      }, completion: { (finished) -> Void in
        UIView.animateWithDuration(0.25, delay: 0.1, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: .CurveEaseOut, animations: {
          self.clockLabel.transform = CGAffineTransformIdentity
          }, completion: {
            (finished) -> Void in
            self.tapGestureRecognizer.enabled = true
        })
    })
  }
}

